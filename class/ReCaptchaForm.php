<?php
class ReCaptchaForm
{
	private $publicKey;
	private $privateKey;
	private $url;
	private $https;
	private $recaptcha;

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
	 * @see https://www.google.com/recaptcha/intro/index.html
	 * @return boolean
	 */
	public function __construct()
	{
		$this->url = 'https://www.google.com/recaptcha/api/siteverify';
		$this->https = false;
		return true;
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
	 * @see https://www.google.com/recaptcha/intro/index.html
	 * @return Void
	 */
	public function hook(): Void
	{
		// adds the captcha to the login form
		add_action( 'login_form', [ $this, 'displayCaptcha' ] );
		add_action( 'register_form', [ $this, 'displayCaptcha' ] );
		add_action( 'lostpassword_form', [ $this, 'displayCaptcha' ] );
		add_action( 'login_enqueue_scripts', [ $this, 'cssCaptcha' ] );

		// authenticate the captcha answer
		add_action( 'wp_authenticate_user', [ $this, 'validateCaptchaLogin' ], 10, 2 );
		add_action( 'registration_errors', [ $this, 'validateCaptchaInsc' ], 10, 2 );
		add_action( 'lostpassword_post', [ $this, 'validateCaptchaInsc' ], 10, 2 );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
	 * @see https://developers.google.com/recaptcha/docs/display#render_param
	 * @return Void
	 */
	public function displayCaptcha(): Void
	{
		echo "<script src='https://www.google.com/recaptcha/api.js'></script>";
		echo "<div class='g-recaptcha' data-theme='ight' data-sitekey='" . $this->publicKey . "' ></div>";
	}

	/**
	 * @author Audrey <a-le@bulko.net>
	 * @since ADVE 1.0.0
	 * @see https://codex.wordpress.org/Customizing_the_Login_Form
	 * @return Void
	 */
	public function cssCaptcha(): Void
	{
		wp_enqueue_style( 'reCaptcha-login', plugins_url( "../css/login.css", __FILE__ ) );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
	 * @param  WP_User $user
	 * @param  String  $password
	 * @return Mixed WP_User || WP_Error
	 */
	public function validateCaptchaLogin( WP_User $user, String $password )
	{
		if( $this->isValidRecaptha() === false )
		{
			return new WP_Error( 'invalid_captcha', 'CAPTCHA response was incorrect');
		}
		return $user;
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
	 * @see https://www.google.com/recaptcha/intro/index.html
	 * @param  WP_Error $errors
	 * @return WP_Error
	 */
	public function validateCaptchaInsc( $errors = null ): WP_Error
	{
		if( $this->isValidRecaptha() === false )
		{
			return new WP_Error( 'invalid_captcha', 'CAPTCHA response was incorrect');
		}
		return $errors;
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @see https://www.google.com/recaptcha/intro/index.html NHAM Controller.php
	 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
	 * @return boolean
	 */
	public function isValidRecaptha(): Bool
	{
		if ( !empty($_POST[ "g-recaptcha-response" ]) )
		{
			$this->recaptcha = $_POST[ "g-recaptcha-response" ];
			$fields = [
				"secret" => $this->privateKey,
				"response" => $this->recaptcha,
			];
			$isHuman = $this->getCurlData( $this->url, $fields );

			if ( $isHuman["success"] )
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
	 * @see NHAM Controller.php
	 * @param  String $url
	 * @param  Array  $fields
	 * @return Array
	 */
	public function getCurlData( String $url, Array $fields ): Array
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		if ( !$this->https )
		{
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		}
		return (array) json_decode( curl_exec($ch) );
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
	 * @param  String  $publicKey
	 */
	public function setPrivate( String $privateKey ): Void
	{
		$this->privateKey = $privateKey;
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
	 * @param  String $privateKey
	 */
	public function setPublic( String $publicKey ): Void
	{
		$this->publicKey = $publicKey;
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
	 * @param  String $url
	 */
	public function setUrl( String $url ): Void
	{
		$this->url = $url;
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
	 * @param  Boolean $https
	 */
	public function setHttps( Bool $https ): Void
	{
		$this->https = $https;
	}

	/**
	 * @author Golga <r-ro@bulko.net>
	 * @since AA
	 * @see https://developers.google.com/recaptcha/docs/faq
	 */
	public function setTestMod(): Void
	{
		$this->setPublic( "6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI" );
		$this->setPrivate( "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe" );
	}
}
